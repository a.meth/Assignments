#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char *argv[])
{
	int rot;
	int y;
	if (getenv("rot")==NULL) {
		rot = 13;
	} else {
		rot = atoi(getenv("rot"));
	}

	char *s = argv[1];
	int x;
	int l = strlen(s);
	for(int i = 0; i < l; i++) {
		if(s[i]>96&&s[i]<123) {
			x = s[i];			
			x = 97+(x+rot-97)%26;
			s[i]=x;
		}
		else if(s[i]>64&&s[i]<91)
    {	x = s[i];
			x = 65+(x+rot-65)%26;
			s[i]=x;
		}
	}
	printf("%s\n",s);
	return 0;
}