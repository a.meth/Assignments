extern scanf
extern printf

section .data
	req: db "Enter your the number",10,0
  big: db "Sorry, too big", 10,0
  corct: db "Yes, correct number!", 10,0
  small: db "Sorry, too small", 10,0
  numtries: db "Number of tries = %d", 10,0
  strg: db "%s",0
  dec: db "%d",0
  stor: dd 0
  inp: dd 0
  num: dd 7
  try: dd 0
section .text

	global main

    main:
    push EBP
	  mov EBP, ESP

    push req
    push strg
    call printf

    push inp
    push dec
    call scanf


    mov EDX, DWORD[num]
    cmp DWORD[inp], EDX
		JG great

		mov EDX, DWORD[num]
    cmp DWORD[inp], EDX
		mov EDX, 0
		mov DWORD[stor], EDX
    JE equal

    mov EDX, DWORD[num]
    cmp DWORD[inp], EDX
    JL less

    P1:
    push req
    push strg
    call printf

    push inp
    push dec
    call scanf

    mov EDX, DWORD[num]
    cmp DWORD[inp], EDX
    JG great
		mov EDX, DWORD[num]
    cmp DWORD[inp], EDX
    JL less
		mov EDX, DWORD[num]
    cmp DWORD[inp], EDX
    JE equal

    count:
    mov EDX, DWORD[inp]
    mov DWORD[stor], EDX
		inc DWORD[try]
    JMP P1

    great:
    push big
    push strg
    call printf
    cmp EDX, DWORD[inp]
    cmp DWORD[stor], EDX
    JNE count
		JMP P1

    less:
    push small
    push strg
    call printf
    mov EDX, DWORD[inp]
    cmp DWORD[stor], EDX
    JNE count
		JMP P1

		equal:
    push corct
    push strg
    call printf

    push DWORD[try]
    push numtries
    call printf

		leave
		ret
